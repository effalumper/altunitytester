package ro.altom.altunitytester.altUnityTesterExceptions;

public class AltUnityRecvallMessageFormatException extends AltUnityRecvallException {

    /**
     *
     */
    private static final long serialVersionUID = -1965738751146487552L;

    public AltUnityRecvallMessageFormatException() {
    }

    public AltUnityRecvallMessageFormatException(String message) {
        super(message);
    }

}
